package calculator;

import java.util.Scanner;


public class Calculator {

    public static void main(String[] args) {
        int a ,b , jawaban, tugas;
        double hasil = 0;
        Scanner masukan = new Scanner(System.in);
        System.out.println("---------------------------------");
        System.out.println("====== C A L C U L A T O R ======");
        do{    
            System.out.println("---------------------------------");
            System.out.println(" 1. Penjumlahan (+) ");
            System.out.println(" 2. Pengurangan (-) ");
            System.out.println(" 3. Perkalian (*) ");
            System.out.println(" 4. Pembagian (/) ");
            System.out.print("Pilih Tugas : ");
            tugas = masukan.nextInt();
            System.out.print("bilangan pertama : ");
            a = masukan.nextInt();
            System.out.print("bilangan kedua : ");
            b = masukan.nextInt();
            double aa = a;
            double bb = b;
            if (tugas == 1)
                hasil = a + b;
            if (tugas == 2)
                hasil = a - b;
            if (tugas == 3)
                hasil = a * b;
            if (tugas == 4)
                hasil = aa / bb;
            System.out.println("Hasil = " + hasil);
            System.out.println("Hitung Ulang ? (Y=1/N=2)");
            jawaban = masukan.nextInt();
        }while (jawaban == 1);
        System.out.println("---------------------------------");
        System.out.println("====== By: Dimas Angkasa N ======");
        System.out.println("---------------------------------");
    }
    
}
